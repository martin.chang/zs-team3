
/*
 * js 基本数据类型：
 *    string number bool null undefined
* */
/*json 对象*/
var user={
    username:'admin',
    password:'123456',
    isUsed:true
}

for (var key in user){
  console.log(key)
}

var users=[
  {
    username:'admin',
    password:'123456',
  },
  {
    username:'test',
    password:'test',
  },{
    username:'test',
    password:'123456',
  }
]

/*
console.log(user.username);
console.log(user)
*/
//console.log(users[1].password);

/*for (var i = 0; i <users.length ; i++) {
  console.log(users[i].username)
}*/

/*for(var key in users){
  console.log(key)
  console.log(users[key].username)
}*/

/*数据过滤*/

var current=  users.find(function (user) {
    return user.username == 'test';
})
//console.log(current);
var finds= users.filter(function (user) {
  return user.password=='123456';
})
console.log(finds)
