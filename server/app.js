/*cnpm install express --save*/
/*cnpm install cors --save*/

var Express = require('express')
var cors=require('cors')
var app = new Express();
app.use(cors());

/*创建服务器端路由*/
var testRouter = require('./router/test')
app.use('/test',testRouter);


var server=app.listen('3000','localhost',function (req,res) {

    var host= server.address().address;
    var post=server.address().port;

    console.log('the server in running on http://%s:%s',host,post);

})
