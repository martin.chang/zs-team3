import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../view/layout/Index'
import Login from '../view/login/Login3'  // 导入组件



import Employee from "../view/basic/Employee";
import DataTest from '../view/0.ex/1.data'
import Diractive from '../view/0.ex/2.diractive'
import login4 from "../view/login/login4";
import order from  "../view/login/order"
import test from "../view/login/test"
import resturants from "../selfProject/resturants"
import register from "../view/login/register"
import  index from "../view/login/index"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/login4',
      name: 'Login4',
      component: login4
    },
    {
      path: '/resturants',
      name: 'Resturants',
      component: resturants
    },
    {
      path: '/index',
      name: 'Index',
      component: index
    },
    {
      path: '/order',
      name: 'Order',
      component: order
    },{
      path: '/register',
      name: 'Register',
      component:register
    },

    {
      path: '/test',
      name: 'Test',
      component: test
    },

    {
      path: '/layout',
      name: 'Layout',
      component: Layout,
      children: [
        {
          path: 'employee',
          name: 'Employee',
          component: Employee
        }
      ]
    },{
         path:'/data',
         name:'Data',
         component:DataTest
    },{
    path:'/directive',
  name:'D',
  component:Diractive
}
  ]
})
