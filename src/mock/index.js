import Mock from 'mockjs'


var employees = Mock.mock({
  'list|5': [{'name': '@cname', 'age': "@natural(20,40)"}]
})

Mock.mock('http://test.org/employee/list', 'get', employees.list);
